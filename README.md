# Symfony 4.4 LTS Base Project + Sonata Admin

## System Requirements

 - PHP >= 7.1.3

 - Composer

 - MySql


## First Install

 - Clone the project

 - Execute `composer install`
 
 - Execute `bin/console ckeditor:install`
 
 - Execute `bin/console assets:install`
  
 - Execute `bin/console doctrine:database:create`
 
 - Execute `bin/console doctrine:schema:update --force`
 
### Create Admin User

 - Execute `bin/console fos:user:create` and complete the information requested by the Wizard
 
 - Execute `bin/console fos:user:promote youadminsusername --super`
 

