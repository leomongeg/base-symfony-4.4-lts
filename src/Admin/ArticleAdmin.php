<?php


namespace App\Admin;


use Behat\Transliterator\Transliterator;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\Form\Validator\ErrorElement;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use Sonata\TranslationBundle\Filter\TranslationFieldFilter;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ArticleAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add('title',  TranslationFieldFilter::class, [
            'filter_mode' => 'default_filter_mode'
        ]);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
//        dump($subject = $this->getSubject()); die;
        $formMapper->add('title', TextType::class)
            ->add('content', SimpleFormatterType::class, [
                'format' => 'richhtml',
                'ckeditor_context' => 'default', // optional
            ]);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('title')
            ->add('slug');
    }

    public function prePersist($object)
    {
        $object->setSlug($this->generateUniqueSlug($object));

        parent::prePersist($object);
    }

    public function preUpdate($object)
    {
        $hasSlug = $this->hasTranslationHasSlug($object);

        if (!$hasSlug) {

            $object->setSlug($this->generateUniqueSlug($object));
        }

        parent::preUpdate($object);
    }

    private function hasTranslationHasSlug($object): ?bool
    {
        $container = $this->getConfigurationPool()->getContainer();
        $em        = $container->get('doctrine.orm.entity_manager');
        $qBuilder  = $em->getRepository('App\Entity\Article\Translation')
            ->createQueryBuilder('T');

        $parameters = [
            ':foreignKey' => $object->getId(),
            ':locale' => $object->getLocale()
        ];

        $qBuilder
            ->select('T.id')
            ->andWhere('T.object = :foreignKey')
            ->andWhere('T.locale = :locale')
            ->setMaxResults(1);

        $qBuilder->setParameters($parameters);

        $result = $qBuilder->getQuery()->execute();

        return count($result) > 0;
    }

    private function generateUniqueSlug($object): ?string
    {
        $slug      = Transliterator::urlize(trim(preg_replace('/\s+/', ' ', $object->getTitle())));
        $container = $this->getConfigurationPool()->getContainer();
        $em        = $container->get('doctrine.orm.entity_manager');
        $qBuilder  = $em->getRepository('App\Entity\Article\Translation')
            ->createQueryBuilder('T');

        $parameters = [
            ':field'   => 'slug',
            ':content' => $slug . '%',
            ':locale'  => $object->getLocale()
        ];

        $qBuilder
            ->select('count(T.id)')
            ->andWhere('T.field = :field')
            ->andWhere('T.content LIKE :content')
            ->andWhere('T.locale = :locale');
        $qBuilder->setParameters($parameters);

        $result = (int)$qBuilder->getQuery()->getSingleScalarResult();

        return $result > 0 ? $slug . '-' . ($result + 1) : $slug;
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        // conditional validation, see the related section for more information
//        if ($object->getEnabled()) {
//
//            // abstract cannot be empty when the post is enabled
//            $errorElement
//                ->with('abstract')
//                ->assertNotBlank()
//                ->assertNotNull()
//                ->end();
//        }
    }
}