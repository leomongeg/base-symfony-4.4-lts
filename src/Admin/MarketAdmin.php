<?php


namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ChoiceFieldMaskType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MarketAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add("name")
            ->add("defaultLocale");
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $locales = $this->getConfigurationPool()->getContainer()->getParameter("sonata_translation.locales");

        $formMapper->add('name', TextType::class)
            ->add("defaultLocale", ChoiceFieldMaskType::class, [
                'choices'     => array_combine(array_map('ucfirst', $locales), $locales),
                'map'         => [
                    'route' => ['route', 'parameters'],
                    'uri'   => ['uri'],
                ],
                'placeholder' => 'Choose an option',
                'required'    => false
            ]);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier("id")
            ->addIdentifier("name")
            ->add("defaultLocale");
    }
}